#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Juneau Hold\nB Anchorage Move Juneau\nC Kodiak Move Nome\nD Nome Move Fairbanks\nE Fairbanks Support A\nF Utqiagvik Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Juneau\nC Nome\nD [dead]\nE [dead]\nF Utqiagvik\n")
    
    def test_solve_2(self):
        r = StringIO("A Detroit Support C\nB Flint Support C \nC Jacksonville Support B\nD Kalamazoo Move Jacksonville\nE Muskegon Move Jacksonville\nF Saginaw Move Flint\nG Petoskey Move Detroit\nH Lansing Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Detroit\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\nG [dead]\nH Lansing\n")

    def test_solve_3(self):
        r = StringIO("A Memphis Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Memphis\n")

    #failure cases
    def test_solve_4(self):
        r = StringIO("A Toronto Hold Quebec City\n")
        w = StringIO()
        self.assertRaises(ValueError,diplomacy_solve,r,w)
    
    def test_solve_5(self):
        r = StringIO("A New York\n")
        w = StringIO()
        self.assertRaises(ValueError,diplomacy_solve,r,w)
        
    
        
'''
# -----------
# TestCollatz
# -----------
class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 1)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 1)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 1)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 1)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 1\n100 200 1\n201 210 1\n900 1000 1\n")
'''


# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
